#pragma once
#include <cassert>
#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <objidl.h>
#include <Unknwn.h>

// GDI PLUS MASSIVE HEADACHE
// Never writing a GUI in C++ ever again
// MASSIVE HEADACHE

// TO DO once project is finished: set up automatic scaling so that it displays properly when screen size isn't large enough
// this will also help with manual screen resizing
#include <gdiplus.h>
#include <CommCtrl.h>
#include <Uxtheme.h>
#pragma comment(lib, "Uxtheme")

class gdiButton
{
	Gdiplus::RectF rect;
	bool selected;
	bool card;
public:
	gdiButton(Gdiplus::RectF rect, bool selected = false, bool card = false);
	Gdiplus::RectF getRectF();
	bool isSelected();
	bool setSelected(bool selected); // returns the new value
	bool isCard();
};

namespace gameStatus
{
	bool isOver;
	bool isPassed[4];
	bool allowInput;
	bool startingTurn;

	int startingPlayer;
	int currentPlayer;
	int cardsSelected;
	int numPlayers; // not implemented yet

	cardTypes currentValidType;

	std::vector<gdiButton>* cardButtons;
	std::vector<gdiButton>* gameButtons;
	std::vector<std::string> messageLog;
	std::vector<cardObject> handAttempt;
}

enum gameButtonTypes
{
	SUBMIT_BUTTON,
	CONTINUE_BUTTON,
	PASS_BUTTON,
	END_ENUM
};

HWND hwndButton1;
HWND hwndButton2;
HRGN messageLogRgn;

// drawing functions
void guiGameState(HDC hdc, HWND hWnd);
void drawButton(HDC hdc, int x, int y, int width, int height, LPCWSTR label, bool selected);
void buttonInvert(HDC hdc, HWND hWnd, gdiButton thisButton, gameButtonTypes buttonType);

// event handling
void draw(HDC hdc, HWND hWnd);
void handleMouseClick(HDC hdc, HWND hWnd);
void humanTurn(bool starting, cardTypes validTypes);

// utility
bool checkEqualRect(Gdiplus::RectF rect1, Gdiplus::RectF rect2);

// game functions (probably will be moved later to ChinesePoker.cpp
void initGame(HDC hdc, HWND hWnd);
void runGame(HDC hdc, HWND hWnd, int numPlayers);
cardTypes submitHumanTurn(HDC hdc, HWND hWnd);
cardTypes submitComputerTurn(HDC hdc, HWND hWnd, std::vector<cardObject>* hand);