#include "ChinesePoker.h"
#include "GUI.h"

using namespace Gdiplus;

// Global variables

// The main window class name.
static TCHAR szWindowClass[] = _T("DesktopApp");

// The string that appears in the application's title bar.
static TCHAR szTitle[] = _T("Chinese Poker");

// Stored instance handle for use in Win32 API calls such as FindResource
HINSTANCE hInst;

// Forward declarations of functions included in this code module:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(
    _In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPSTR     lpCmdLine,
    _In_ int       nCmdShow
)
{
    WNDCLASSEX wcex;

    GdiplusStartupInput gdiplusStartupInput;
    ULONG_PTR gdiplusToken;
    GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(wcex.hInstance, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = szWindowClass;
    wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

    if (!RegisterClassEx(&wcex))
    {
        MessageBox(NULL,
            _T("Call to RegisterClassEx failed!"),
            _T("Chinese Poker"),
            NULL);

        return 1;
    }

    // Store instance handle in our global variable
    hInst = hInstance;

    // The parameters to CreateWindowEx explained:
    // WS_EX_OVERLAPPEDWINDOW : An optional extended window style.
    // szWindowClass: the name of the application
    // szTitle: the text that appears in the title bar
    // WS_OVERLAPPEDWINDOW: the type of window to create
    // CW_USEDEFAULT, CW_USEDEFAULT: initial position (x, y)
    // 1330, 800: initial size (width, length)
    // NULL: the parent of this window
    // NULL: this application does not have a menu bar
    // hInstance: the first parameter from WinMain
    // NULL: not used in this application
    HWND hWnd = CreateWindowEx(
        WS_EX_OVERLAPPEDWINDOW,
        szWindowClass,
        szTitle,
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        1330, 800,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    if (!hWnd)
    {
        MessageBox(NULL,
            _T("Call to CreateWindow failed!"),
            _T("Chinese Poker"),
            NULL);

        return 1;
    }

    hwndButton1 = CreateWindow(
        L"BUTTON",  // Predefined class; Unicode assumed 
        L"Start game",      // Button text 
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
        5,         // x position 
        5,         // y position 
        100,        // Button width
        50,        // Button height
        hWnd,     // Parent window
        (HMENU) 1,       // wParam value.
        (HINSTANCE)GetWindowLong(hWnd, (int)hInstance),
        NULL);      // Pointer not needed.

    // The parameters to ShowWindow explained:
    // hWnd: the value returned from CreateWindow
    // nCmdShow: the fourth parameter from WinMain
    ShowWindow(hWnd,
        nCmdShow);
    UpdateWindow(hWnd);

    // Main message loop:
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    GdiplusShutdown(gdiplusToken);

    return (int)msg.wParam;
}

//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;
    RECT rc;
    HDC memdc;
    HPAINTBUFFER hbuff;

    switch (message)
    {
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);
        // this buffered paint prevents flickering
        GetClientRect(hWnd, &rc);
        hbuff = BeginBufferedPaint(hdc, &rc, BPBF_COMPATIBLEBITMAP, NULL, &memdc);

        draw(memdc, hWnd);

        EndBufferedPaint(hbuff, TRUE);
        EndPaint(hWnd, &ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    case WM_COMMAND: // start button press
        hdc = BeginPaint(hWnd, &ps);

        if (wParam == 1)
            guiGameState(hdc, hWnd);

        initGame(hdc, hWnd);
        EndPaint(hWnd, &ps);
        break;
    case WM_LBUTTONDOWN: // detect card selections
        hdc = BeginPaint(hWnd, &ps);
        handleMouseClick(hdc, hWnd);
        EndPaint(hWnd, &ps);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
        break;
    }

    return 0;
}

void guiGameState(HDC hdc, HWND hWnd)
{
    DestroyWindow(hwndButton1);
}

void handleMouseClick(HDC hdc, HWND hWnd)
{   
    if (!(gameStatus::gameButtons))
    {
        gameStatus::gameButtons = new std::vector<gdiButton>;
        return;
    }
    if (!(gameStatus::cardButtons))
    {
        gameStatus::cardButtons = new std::vector<gdiButton>;
        return;
    }

    POINT cursorPos;
    GetCursorPos(&cursorPos);

    if (ScreenToClient(hWnd, &cursorPos))
    {
        // game buttons
        // a lot of the game flow happens here
        bool updateCard = false;
        RectF storeRect;

        if (gameStatus::startingPlayer && gameStatus::gameButtons->size() > 0)
        {
            int staticCurrentPlayer = gameStatus::currentPlayer; // without this the loop will get messed up by currentPlayer incrementing
            for (int i = SUBMIT_BUTTON; i < END_ENUM; i++)
            {
                RectF buttonRect = gameStatus::gameButtons->at(i).getRectF();
                if (cursorPos.x > buttonRect.GetLeft() && cursorPos.x < buttonRect.GetRight()
                    && cursorPos.y > buttonRect.GetTop() && cursorPos.y < buttonRect.GetBottom())
                {
                    if (staticCurrentPlayer == 1 && i == CONTINUE_BUTTON)
                        continue;
                    if (staticCurrentPlayer != 1 && i == SUBMIT_BUTTON)
                        continue;

                    gameStatus::gameButtons->at(i).setSelected(true);

                    // if any more buttons are added a switch would be easier
                    if (i == SUBMIT_BUTTON) // player turn
                        gameStatus::currentValidType = submitHumanTurn(hdc, hWnd);
                    else if (i == CONTINUE_BUTTON) // skip all passed players with one continue press
                    {
                        while (gameStatus::isPassed[gameStatus::currentPlayer - 1])
                        {
                            if (gameStatus::currentPlayer == 4)
                            {
                                gameStatus::currentPlayer = 1;
                                if (!gameStatus::isPassed[0])
                                    gameStatus::allowInput = true;
                                else
                                    gameStatus::currentPlayer++;
                            }
                            else
                                gameStatus::currentPlayer++;
                        }
                        if (gameStatus::currentPlayer > 1)
                            gameStatus::currentValidType = submitComputerTurn(hdc, hWnd, globalHands::allPlayerHands.at(gameStatus::currentPlayer - 1));

                        bool shouldFreebie = true;
                        for (int j = gameStatus::numPlayers; j < 4; j++)
                            if (!gameStatus::isPassed[j])
                                shouldFreebie = false;

                        if (shouldFreebie && gameStatus::currentPlayer == 1)
                            gameStatus::messageLog.push_back(std::string("It's your freebie. You can play any card or valid hand of any size."));
                    }
                    else if (i == PASS_BUTTON && staticCurrentPlayer == 1)
                    {
                        bool shouldFreebie = true;
                        for (int j = gameStatus::numPlayers; j < 4; j++)
                            if (!gameStatus::isPassed[j])
                                shouldFreebie = false;
                            
                        if (shouldFreebie || gameStatus::startingTurn)
                            gameStatus::messageLog.push_back(std::string("You can't pass on your own freebie!"));
                        else
                        {
                            gameStatus::messageLog.push_back(std::string("You passed."));
                            gameStatus::currentPlayer++;
                            gameStatus::isPassed[0] = true;
                        }
                    }
                }
            }
        }

        // card buttons (player hand)
        for (size_t i = 0; i < gameStatus::cardButtons->size(); i++)
        {
            RectF buttonRect = gameStatus::cardButtons->at(i).getRectF();

            if (cursorPos.x > buttonRect.GetLeft() && cursorPos.x < buttonRect.GetRight()
                && cursorPos.y > buttonRect.GetTop() && cursorPos.y < buttonRect.GetBottom()
                && gameStatus::allowInput)
            {
                bool isSelected = gameStatus::cardButtons->at(i).isSelected();
                updateCard = true;
                storeRect = buttonRect;

                if (gameStatus::cardsSelected >= 5 && !isSelected)
                {
                    gameStatus::messageLog.push_back(std::string("You can't select more than 5 cards!"));
                }
                else
                {
                    gameStatus::cardButtons->at(i).setSelected(!isSelected);
                    gameStatus::cardsSelected += (gameStatus::cardButtons->at(i).isSelected()) ? 1 : -1;
                    if (gameStatus::cardButtons->at(i).isSelected())
                    {
                        gameStatus::handAttempt.push_back(globalHands::hand1.at(i));
                        sortHand(gameStatus::handAttempt);
                    }
                    else
                    {
                        for (size_t j = 0; j < gameStatus::handAttempt.size(); j++)
                        {
                            cardObject card = gameStatus::handAttempt.at(j);
                            if (isSameCard(card, globalHands::hand1.at(i)))
                                gameStatus::handAttempt.erase(gameStatus::handAttempt.begin() + j);
                        }
                    }
                }
            }
        }

        // only invalidate the card that just changed to prevent flickering, need to include new + old area
        // also invalidate message log
        //HRGN cardRgn = CreateRectRgn(storeRect.GetLeft(), storeRect.GetTop(), storeRect.GetRight(), storeRect.GetBottom() + 25);
        //InvalidateRgn(hWnd, cardRgn, TRUE);
        //InvalidateRgn(hWnd, messageLogRgn, TRUE);

        RECT clientRect;
        GetClientRect(hWnd, &clientRect);
        HRGN clientRgn = CreateRectRgn(0, 0, clientRect.right, clientRect.bottom);

        InvalidateRgn(hWnd, clientRgn, TRUE);
    }
    else
        throw 1; // unable to get client coords
}

// all drawing will be done in this function
void draw(HDC hdc, HWND hWnd)
{
    Graphics gM(hdc);

    RECT clientRect;
    GetClientRect(hWnd, &clientRect);
    RectF clientRectF = RectF(clientRect.left, clientRect.top, (clientRect.right - clientRect.left), (clientRect.bottom - clientRect.top));
    SolidBrush cBrush(Color(255, 255, 255));
    gM.FillRectangle(&cBrush, clientRectF);
    

    FontFamily fontFamily(L"Calibri");
    Font       font(&fontFamily, 12);
    Font       boldFont(&fontFamily, 12, FontStyleBold);
    Font       largeFont(&fontFamily, 24, FontStyleBold);

    FontFamily rFontFam(L"Smallest Pixel-7");
    Font       rBoldFont(&rFontFam, 24, FontStyleBold);

    int cardWidth = 100;
    int cardHeight = 140;

    if (!(gameStatus::cardButtons))
        gameStatus::cardButtons = new std::vector<gdiButton>;
    if (!(gameStatus::gameButtons))
        gameStatus::gameButtons = new std::vector<gdiButton>;

#pragma region top misc
    LPCWSTR greeting = L"Current Player: ";
    TextOut(hdc, 5, 60, greeting, wcslen(greeting));
    //LPCWSTR startingPlayerCStr;

    std::string playerString;

    if (gameStatus::startingPlayer)
        playerString = (gameStatus::currentPlayer == 1) ? "You!" : ("Computer " + std::to_string(gameStatus::currentPlayer - 1));
    else
        playerString = std::string();

    std::wstring wPlayerString = std::wstring(playerString.begin(), playerString.end());
    LPCWSTR lPlayerString = wPlayerString.c_str();
    TextOut(hdc, 110, 60, lPlayerString, wcslen(lPlayerString));

    if (gameStatus::numPlayers)
    {
        std::string rotaryText;
        float xPos = 200.f;
        float yPos = 60.f;
        
        for (int i = 1; i < 5; i++)
        {
            rotaryText = (i == 1) ? std::string("P1") : std::string("CPU") + std::to_string(i - 1);

            cBrush.SetColor(!(gameStatus::isPassed[i - 1]) ? gameStatus::currentPlayer == i ?
                Color(7, 123, 16) : // current player
                Color(0, 0, 0) : // not current not passed
                Color(145, 28, 19)); // passed

            PointF origin = PointF(xPos, yPos);

            std::wstring wRotaryStr = std::wstring(rotaryText.begin(), rotaryText.end());
            LPCWSTR lRotaryStr = wRotaryStr.c_str();
            gM.DrawString(lRotaryStr, wcslen(lRotaryStr), &rBoldFont, origin, &cBrush);

            RectF widthRect;
            gM.MeasureString(lRotaryStr, wcslen(lRotaryStr), &rBoldFont, origin, &widthRect);
            xPos += widthRect.Width + 2.f;
        }
    }
#pragma endregion

#pragma region top banner
    if (gameStatus::startingPlayer)
    {
        // game buttons are individually registered
        RectF submitRectF = RectF(5, 5, 200, 50);
        RectF continueRectF = RectF(5, 5, 200, 50);
        RectF passRectF = RectF(210, 5, 200, 50);
        
        // only button display is handled here, which button press to handle is done in handleMouseClick(). all buttons are pushed back to gameButtons.
        // remember to change enum so it's easier to understand
        if (!gameStatus::gameButtons->size())
        {
            gdiButton submitButton = gdiButton(submitRectF);
            gameStatus::gameButtons->push_back(submitButton);
            gdiButton continueButton = gdiButton(continueRectF);
            gameStatus::gameButtons->push_back(continueButton);
            gdiButton passButton = gdiButton(passRectF);
            gameStatus::gameButtons->push_back(passButton);
        }
        
        if (gameStatus::currentPlayer == 1)
        {
            drawButton(hdc, submitRectF.X, submitRectF.Y, submitRectF.Width, submitRectF.Height, L"Submit Hand", gameStatus::gameButtons->at(SUBMIT_BUTTON).isSelected());
            buttonInvert(hdc, hWnd, gameStatus::gameButtons->at(SUBMIT_BUTTON), SUBMIT_BUTTON);
        }
        else
        {
            drawButton(hdc, continueRectF.X, continueRectF.Y, continueRectF.Width, continueRectF.Height, L"Continue", gameStatus::gameButtons->at(CONTINUE_BUTTON).isSelected());
            buttonInvert(hdc, hWnd, gameStatus::gameButtons->at(CONTINUE_BUTTON), CONTINUE_BUTTON);
        }

        drawButton(hdc, passRectF.X, passRectF.Y, passRectF.Width, passRectF.Height, L"Pass", gameStatus::gameButtons->at(PASS_BUTTON).isSelected());
        buttonInvert(hdc, hWnd, gameStatus::gameButtons->at(PASS_BUTTON), PASS_BUTTON);
        

        TextOut(hdc, 415, 22, L"Hand to submit:", wcslen(L"Hand to submit:"));
        if (gameStatus::handAttempt.size() > 0)
        {
            // scale down card images, use this method to scale entire GUI later on
            float scalingFactor = 0.5f;
            int xPos = 550;
            int yPos = 5;
            
            for (cardObject card : gameStatus::handAttempt)
            {
                std::string prefix = "C:\\Users\\Bendy\\Downloads\\CARD IMAGES\\";
                std::string fileName = prefix + card.getFileName();
                std::wstring wFileName = std::wstring(fileName.begin(), fileName.end());
                LPCWSTR lFileName = wFileName.c_str();

                Image* cardImgUnscaled = new Image(lFileName, FALSE);
                float newWidth = scalingFactor * (float)cardImgUnscaled->GetWidth();
                float newHeight = scalingFactor * (float)cardImgUnscaled->GetHeight();
                Image* scaledImg = new Bitmap((int)newWidth, (int)newHeight);

                Graphics gT(scaledImg);
                gT.ScaleTransform(scalingFactor, scalingFactor);
                gT.DrawImage(cardImgUnscaled, 0, 0); // scaledImg is now a scaled version of cardImgUnscaled

                gM.DrawImage(scaledImg, xPos, yPos);

                //DeleteObject(cardImgUnscaled);
                //DeleteObject(scaledImg);

                delete cardImgUnscaled;
                delete scaledImg;

                xPos += newWidth;
            }
        }
    }
#pragma endregion

#pragma region last hand
    TextOut(hdc, 5, 80, L"Last hand played:", wcslen(L"Last hand played:"));

    if (globalHands::handHistory)
    {
        if (globalHands::handHistory->size())
        {
            int xPos = 5;
            int yPos = 100;

            for (cardObject card : globalHands::handHistory->back())
            {
                // holy strings
                std::string prefix = "C:\\Users\\Bendy\\Downloads\\CARD IMAGES\\";
                std::string fileName = prefix + card.getFileName();
                std::wstring wFileName = std::wstring(fileName.begin(), fileName.end());
                LPCWSTR lFileName = wFileName.c_str();

                Image* cardImg = new Image(lFileName, FALSE);
                gM.DrawImage(cardImg, xPos, yPos);

                delete cardImg;

                // no buttons in hand history

                xPos += cardWidth;
            }
        }
    }
#pragma endregion

#pragma region player hand
    int selectedShift = 25; // how much each card img moves up when selected
    TextOut(hdc, 5, 270, L"Your hand:", wcslen(L"Your hand:"));

    if (globalHands::hand1.size())
    {
        int xPos = 5;
        int yPos = 315;

        for (cardObject card : globalHands::hand1)
        {
            // holy strings
            std::string prefix = "C:\\Users\\Bendy\\Downloads\\CARD IMAGES\\";
            std::string fileName = prefix + card.getFileName();
            std::wstring wFileName = std::wstring(fileName.begin(), fileName.end());
            LPCWSTR lFileName = wFileName.c_str();

            Image* cardImg = new Image(lFileName, FALSE);
            RectF cardRect = RectF(xPos, yPos, cardWidth, cardHeight);

            bool foundCard = false;
            size_t activeIndex = -1;

            for (size_t i = 0; i < gameStatus::cardButtons->size(); i++)
            {
                if (checkEqualRect(cardRect, gameStatus::cardButtons->at(i).getRectF()))
                {
                    foundCard = true;
                    if (gameStatus::cardButtons->at(i).isSelected())
                        activeIndex = i;
                }
            }

            if (!foundCard)
            {
                gdiButton cardButton = gdiButton(cardRect, false, true);
                gameStatus::cardButtons->push_back(cardButton);
            }
            else if (activeIndex != -1) // card is currently selected so move it down
            {
                yPos -= 25;
            }

            gM.DrawImage(cardImg, xPos, yPos);

            //DeleteObject(cardImg);
            
            delete cardImg;

            xPos += cardWidth;
            if (activeIndex != -1) // move yPos back up for the next card
                yPos += 25;
        }
    }
#pragma endregion

#pragma region message log
    TextOut(hdc, 5, 460, L"Message log:", wcslen(L"Message log:"));
    Pen pen(Color(0, 0, 0));
    SolidBrush brush(Color(0, 0, 0));

    gM.DrawRectangle(&pen, 2, 457, 800, 296);
    gM.DrawLine(&pen, 2, 477, 802, 477);

    float xPos = 5.f, left = xPos;
    float yPos = 477.f, top = yPos;

    while (gameStatus::messageLog.size() > 14) // fix this size later
        gameStatus::messageLog.erase(gameStatus::messageLog.begin());

    for (size_t i = 0; i < gameStatus::messageLog.size(); i++)
    {
        std::string message = gameStatus::messageLog.at(i);
        std::wstring wMessage = std::wstring(message.begin(), message.end());
        LPCWSTR lMessage = wMessage.c_str();

        PointF origin = PointF(xPos, yPos);

        gM.DrawString(lMessage, wcslen(lMessage), &font, origin, &brush);

        yPos += 20.f;
    }

    float right = 802.f;
    float bottom = yPos;

    messageLogRgn = CreateRectRgn(left, top, right, bottom);

#pragma endregion

#pragma region status box
    TextOut(hdc, 810, 460, L"Player status:", wcslen(L"Player status:"));

    gM.DrawRectangle(&pen, 807, 457, 250, 200);
    gM.DrawLine(&pen, 807, 477, 1057, 477);

    xPos = 810.f;
    yPos = 477.f;
    // PLAYER
    std::string playerOneString("Player 1 (");
    playerOneString += std::to_string(globalHands::hand1.size()) + std::string(")");
    playerOneString += (gameStatus::isPassed[0]) ? " PASSED" : "";

    std::string statusLine = playerOneString;
    std::wstring wStatus = std::wstring(statusLine.begin(), statusLine.end());
    LPCWSTR lStatus = wStatus.c_str();

    PointF origin = PointF(xPos, yPos);

    gM.DrawString(lStatus, wcslen(lStatus), &boldFont, origin, &brush);

    yPos += 20.f;
    // COMPUTER 1
    std::string compOneString("Computer 1 (");
    compOneString += std::to_string(globalHands::hand2.size()) + std::string(")");
    compOneString += (gameStatus::isPassed[1]) ? " PASSED" : "";

    statusLine = compOneString;
    wStatus = std::wstring(statusLine.begin(), statusLine.end());
    lStatus = wStatus.c_str();

    origin = PointF(xPos, yPos);

    gM.DrawString(lStatus, wcslen(lStatus), &boldFont, origin, &brush);
    
    yPos += 20.f;
    // COMPUTER 2
    std::string compTwoString("Computer 2 (");
    compTwoString += std::to_string(globalHands::hand3.size()) + std::string(")");
    compTwoString += (gameStatus::isPassed[2]) ? " PASSED" : "";

    statusLine = compTwoString;
    wStatus = std::wstring(statusLine.begin(), statusLine.end());
    lStatus = wStatus.c_str();

    origin = PointF(xPos, yPos);

    gM.DrawString(lStatus, wcslen(lStatus), &boldFont, origin, &brush);

    yPos += 20.f;
    // COMPUTER 3
    std::string compThreeString("Computer 3 (");
    compThreeString += std::to_string(globalHands::hand4.size()) + std::string(")");
    compThreeString += (gameStatus::isPassed[3]) ? " PASSED" : "";

    statusLine = compThreeString;
    wStatus = std::wstring(statusLine.begin(), statusLine.end());
    lStatus = wStatus.c_str();

    origin = PointF(xPos, yPos);

    gM.DrawString(lStatus, wcslen(lStatus), &boldFont, origin, &brush);
    // resume here
#pragma endregion
}

// TO DO: add RectF overload to simplify
void drawButton(HDC hdc, int x, int y, int width, int height, LPCWSTR label, bool selected)
{
    Graphics     gM(hdc);
    Pen          pen(Color(0, 0, 0));
    SolidBrush   brush(Color(230, 230, 230));

    float xFloat = static_cast<float>(x);
    float yFloat = static_cast<float>(y);

    FontFamily   fontFamily(L"Cambria");
    Font         font(&fontFamily, 12, FontStyleBold);
    RectF        rectF(xFloat, yFloat, width, height);
    StringFormat stringFormat;

    if (selected)
    {
        pen.SetColor(Color(255, 255, 255));
        brush.SetColor(Color(25, 25, 25));
    }

    gM.DrawRectangle(&pen, x, y, width, height);
    gM.FillRectangle(&brush, x + 1, y + 1, width - 1, height - 1);
    gM.DrawRectangle(&pen, x + 2, y + 2, width - 4, height - 4); // double border

    stringFormat.SetAlignment(StringAlignmentCenter);
    stringFormat.SetLineAlignment(StringAlignmentCenter);

    if (selected)
        brush.SetColor(Color(255, 255, 255));
    else
        brush.SetColor(Color(0, 0, 0));

    gM.DrawString(label, -1, &font, rectF, &stringFormat, &brush);
}

void buttonInvert(HDC hdc, HWND hWnd, gdiButton thisButton, gameButtonTypes buttonType)
{
    if (gameStatus::gameButtons->at(buttonType).isSelected())
        gameStatus::gameButtons->at(buttonType).setSelected(false);
    else
        return;

    //std::this_thread::sleep_for(std::chrono::milliseconds(20));

    RECT clientRect;
    GetClientRect(hWnd, &clientRect);
    HRGN clientRgn = CreateRectRgn(0, 0, clientRect.right, clientRect.bottom);

    InvalidateRgn(hWnd, clientRgn, TRUE); // invalidate will cause a WM_PAINT call, repaint the entire window here

    //HRGN buttonRgn = CreateRectRgn(thisButton.getRectF().GetLeft(), thisButton.getRectF().GetTop(), thisButton.getRectF().GetRight() + 4, thisButton.getRectF().GetBottom() + 4);
    //InvalidateRgn(hWnd, buttonRgn, TRUE);
}








// -------------------------------------------------------------------------------------------------------------------------------------- TO BE MOVED

void initGame(HDC hdc, HWND hWnd)
{
    std::srand(std::time(NULL));

    dealCards();
    runGame(hdc, hWnd, 1);
}

// this is where game display updates will take place
void runGame(HDC hdc, HWND hWnd, int numPlayers)
{
    // initialize global variables
    gameStatus::isOver = false;
    gameStatus::startingPlayer = 0;
    gameStatus::startingTurn = true;
    gameStatus::numPlayers = 1; // number of human players, not total players

    for (int i = 0; i < 4; i++)
        gameStatus::isPassed[i] = false;

    if (!gameStatus::cardButtons)
        gameStatus::cardButtons = new std::vector<gdiButton>;
    if (!gameStatus::gameButtons)
        gameStatus::gameButtons = new std::vector<gdiButton>;

    gameStatus::messageLog.push_back(std::string("Welcome to Chinese Poker!"));
    
    std::vector<std::vector<cardObject>*> allHands;
    allHands.push_back(&globalHands::hand1);
    allHands.push_back(&globalHands::hand2);
    allHands.push_back(&globalHands::hand3);
    allHands.push_back(&globalHands::hand4);

    gameStatus::startingPlayer = findStartingPlayer(allHands);
    gameStatus::currentPlayer = gameStatus::startingPlayer;
    gameStatus::cardsSelected = 0;

    std::string playerString = (gameStatus::currentPlayer == 1) ? "Player 1" : ("Computer " + std::to_string(gameStatus::currentPlayer - 1));
    gameStatus::messageLog.push_back(std::string("The starting player is player ") + playerString + std::string("."));

    gameStatus::currentValidType = cardTypes::FREEBIE;

    RECT clientRect;
    GetClientRect(hWnd, &clientRect);
    HRGN clientRgn = CreateRectRgn(0, 0, clientRect.right, clientRect.bottom);

    InvalidateRgn(hWnd, clientRgn, TRUE); // invalidate will cause a WM_PAINT call, repaint the entire window here

    if (gameStatus::startingPlayer == 1)
    {
        gameStatus::allowInput = true;
        humanTurn(true, gameStatus::currentValidType);
    }
    else
    {
        gameStatus::allowInput = false;
    }
}

cardTypes submitHumanTurn(HDC hdc, HWND hWnd)
{
    bool shouldFreebie = true;
    for (int i = 1; i < 4; i++)
        if (!gameStatus::isPassed[i])
            shouldFreebie = false;

    if (shouldFreebie)
        gameStatus::currentValidType = FREEBIE;

    comboTypes validTurn = isValidTurn(gameStatus::handAttempt, gameStatus::startingTurn, gameStatus::currentValidType);
    int turnSize = 0;
    if (!validTurn)
    {
        if (!(gameStatus::handAttempt.size() > 0 && gameStatus::handAttempt.size() <= 5 && gameStatus::handAttempt.size() != 4))
            gameStatus::messageLog.push_back(std::string("Invalid hand size! Must be 1, 2, 3, or 5 cards."));
        else if (shouldFreebie && !gameStatus::handAttempt.size())
            gameStatus::messageLog.push_back(std::string("You have to play a card on your own freebie!"));
        else
            gameStatus::messageLog.push_back(std::string("The cards you chose do not form a valid hand, or your hand is not the correct size."));
        return gameStatus::currentValidType;
    }
    else
    {
        if (gameStatus::currentValidType == FREEBIE && !gameStatus::startingTurn)
            for (int i = 0; i < 4; i++)
                gameStatus::isPassed[i] = false;
        
        if (!gameStatus::startingTurn && globalHands::handHistory && gameStatus::currentValidType != FREEBIE)
        {
            if (gameStatus::handAttempt < globalHands::handHistory->back())
            {
                gameStatus::messageLog.push_back(std::string("The hand you played does not beat the previous hand."));
                return gameStatus::currentValidType;
            }
        }
        else if (!globalHands::handHistory)
            globalHands::handHistory = new std::vector<std::vector<cardObject>>;

        // if you made it here, congrats your hand is valid
        globalHands::handHistory->push_back(gameStatus::handAttempt);

        for (size_t i = 0; i < gameStatus::handAttempt.size(); i++)
            globalHands::hand1.erase(globalHands::hand1.begin() + vectorIndexOf(globalHands::hand1, gameStatus::handAttempt.at(i)));

        turnSize = gameStatus::handAttempt.size();
        gameStatus::handAttempt.clear();

        std::string message = "You played a ";
        switch (validTurn)
        {
        case NON_COMBO:
            switch (turnSize)
            {
            case 1:
                message += "single card.";
                break;
            case 2:
                message += "double.";
                break;
            case 3:
                message += "triple.";
                break;
            }
            break;
        case FLUSH:
            message += "flush.";
            break;
        case STRAIGHT:
            message += "straight.";
            break;
        case FULL_HOUSE:
            message += "full house.";
            break;
        case BOMB:
            message += "bomb.";
            break;
        case ROYAL_FLUSH:
            message += "STRAIGHT FLUSH.";
            break;
        }

        gameStatus::messageLog.push_back(message);
        if (gameStatus::startingTurn)
            gameStatus::startingTurn = false;

        gameStatus::currentPlayer++;
        gameStatus::allowInput = false;

        // invalidate to update hand attempt display
        // clear cardButtons and cardsSelected before updating display so they stay synced with each other and draw()
        gameStatus::cardButtons->clear();
        gameStatus::cardsSelected = 0;
        RECT clientRect;
        GetClientRect(hWnd, &clientRect);
        HRGN clientRgn = CreateRectRgn(0, 0, clientRect.right, clientRect.bottom);

        InvalidateRgn(hWnd, clientRgn, TRUE);
    }
    return (validTurn > NON_COMBO) ? COMBO : (cardTypes)(turnSize - 1); // convert hand result back to cardTypes
}

cardTypes submitComputerTurn(HDC hdc, HWND hWnd, std::vector<cardObject>* hand)
{
    while (gameStatus::isPassed[gameStatus::currentPlayer - 1])
    {
        if (gameStatus::currentPlayer == 4)
        {
            gameStatus::currentPlayer = 1;
            if (!gameStatus::isPassed[0])
                gameStatus::allowInput = true;
            else
                gameStatus::currentPlayer++;
        }
        else
            gameStatus::currentPlayer++;

        return gameStatus::currentValidType;
    }

    bool shouldSaveCard = false;
    int numTwos = 0;
    int numAces = 0;
    cardObject savedHighCard;
    bool shouldFreebie = true;

    for (int i = 0; i < 4; i++)
        if (!gameStatus::isPassed[i] && i != (gameStatus::currentPlayer - 1))
            shouldFreebie = false;

    if (shouldFreebie)
        gameStatus::currentValidType = FREEBIE;

    for (cardObject card : *hand)
        (card.getValue() == 0) ? numTwos++ : (card.getValue() == 13) ? numAces++ : 1;

    if (!gameStatus::startingTurn) // survey playing possibilities and try to save at least one high card for late freebies if needed
    {
        std::vector<cardObject> trialHand = *hand;
        std::vector<cardObject> testAttempts = lowestValidHandOfType(trialHand, FREEBIE, true, false);

        while (testAttempts.size() > 1)
        {
            for (cardObject card : testAttempts)
                trialHand.erase(trialHand.begin() + vectorIndexOf(trialHand, card));

            testAttempts = lowestValidHandOfType(trialHand, FREEBIE, true, false);
        }
        if (((trialHand.size() > 0 && trialHand.back().getValue() < 12 && trialHand.back().getValue() != 0) 
            || trialHand.size() > 2) && hand->size() > 2
            || !trialHand.size()) // for now only hold on to an ace or higher if needed, can experiment with looking through handhistory later
        {
            shouldSaveCard = true;
            savedHighCard = hand->back();
            hand->pop_back();
        }
    }

    bool opponentPotentialWin = false;
    for (std::vector<cardObject>* playerHand : globalHands::allPlayerHands)
        if (playerHand->size() == 1)
            opponentPotentialWin = true;

    std::vector<cardObject> lowHand;
    if (gameStatus::startingTurn) // error stems from not detecting doubles or triples yet
        lowHand = takeStartingTurn(*hand);
    else if (gameStatus::currentValidType == FREEBIE) // set ignoreHistory to true
    {
        lowHand = lowestValidHandOfType(*hand, gameStatus::currentValidType, true, true);
        for (int i = 0; i < 4; i++)
            gameStatus::isPassed[i] = false;
    }
    else
        lowHand = lowestValidHandOfType(*hand, gameStatus::currentValidType, false, true);

    if (!lowHand.size())
    {
        if (shouldSaveCard && (opponentPotentialWin || hand->size() < 3))
        {
            hand->push_back(savedHighCard);
            shouldSaveCard = false;
        }
        if (hand->size() <= (2 * numTwos) || opponentPotentialWin)
            lowHand = lowestValidHandOfType(*hand, gameStatus::currentValidType, shouldFreebie, false); // only cards left are 2s or no valid hands available

        if (!lowHand.size())
            lowHand = lowestValidHandOfType(*hand, gameStatus::currentValidType, shouldFreebie, true);
    }

    if (shouldSaveCard)
        hand->push_back(savedHighCard);
    
    comboTypes turnType = isValidTurn(lowHand, gameStatus::startingTurn, gameStatus::currentValidType);

    if (!lowHand.size())
    {
        std::string message = "Computer " + std::to_string(gameStatus::currentPlayer - 1) + " passed.";
        gameStatus::messageLog.push_back(std::string(message));
        gameStatus::isPassed[gameStatus::currentPlayer - 1] = true;

        if (gameStatus::currentPlayer == 4)
        {
            gameStatus::currentPlayer = 1;
            if (!gameStatus::isPassed[0])
                gameStatus::allowInput = true;
            else
                gameStatus::currentPlayer++;
        }
        else
            gameStatus::currentPlayer++;

        return gameStatus::currentValidType;
    }
    else if (lowHand.size() == (gameStatus::currentValidType == COMBO) ? 5 : gameStatus::currentValidType + 1) // correct turn size
    {
        if (turnType == INVALID)
        {
            gameStatus::messageLog.push_back(std::string("Invalid turn attempt by computer."));
            throw 1;
        }
        for (size_t i = 0; i < lowHand.size(); i++)
            hand->erase(hand->begin() + vectorIndexOf(*hand, lowHand.at(i)));
        globalHands::handHistory->push_back(lowHand);
    }
    else
    {
        gameStatus::messageLog.push_back(std::string("Invalid turn size by computer (") + std::to_string(lowHand.size()) + std::string(" cards)."));
        throw 2;
    }

    std::string message = "Computer " + std::to_string(gameStatus::currentPlayer - 1) + " played a ";
    switch (turnType)
    {
    case NON_COMBO:
        switch (lowHand.size())
        {
        case 1:
            message += "single card.";
            break;
        case 2:
            message += "double.";
            break;
        case 3:
            message += "triple.";
            break;
        }
        break;
    case FLUSH:
        message += "flush.";
        break;
    case STRAIGHT:
        message += "straight.";
        break;
    case FULL_HOUSE:
        message += "full house.";
        break;
    case BOMB:
        message += "bomb.";
        break;
    case ROYAL_FLUSH:
        message += "STRAIGHT FLUSH.";
        break;
    }

    gameStatus::messageLog.push_back(message);

    if (gameStatus::startingTurn)
        gameStatus::startingTurn = false;

    if (gameStatus::currentPlayer == 4)
    {
        gameStatus::currentPlayer = 1;
        if (!gameStatus::isPassed[0])
            gameStatus::allowInput = true;
        else
            gameStatus::currentPlayer++;
    }
    else
        gameStatus::currentPlayer++;

    // invalidate to update hand history display and message box
    RECT clientRect;
    GetClientRect(hWnd, &clientRect);
    HRGN clientRgn = CreateRectRgn(0, 0, clientRect.right, clientRect.bottom);

    InvalidateRgn(hWnd, clientRgn, TRUE);

    return (turnType > NON_COMBO) ? COMBO : (cardTypes)(lowHand.size() - 1);
}

// much easier to get human turn with a gui now, simply loop through card buttons vector and add to attempted hand
// attempted hand is drawn in draw() like all other drawing
void humanTurn(bool starting, cardTypes validTypes)
{
    if (starting)
       gameStatus::messageLog.push_back(std::string("You are starting. You can play any valid hand that includes the 3 of diamonds."));
    else if (validTypes == FREEBIE)
        gameStatus::messageLog.push_back(std::string("All computers passed so you have a freebie. You can play any valid hand."));
}
// -------------------------------------------------------------------------------------------------------------------------------------- TO BE MOVED ^^^^^^^^^^^^^^










bool checkEqualRect(RectF rect1, RectF rect2)
{
    return (rect1.X == rect2.X && rect1.Y == rect2.Y && rect1.Width == rect2.Width && rect1.Height == rect2.Height);
}

gdiButton::gdiButton(RectF rect, bool selected, bool card)
{
    this->rect = rect;
    this->selected = selected;
    this->card = card;
}

RectF gdiButton::getRectF()
{
    return this->rect;
}

bool gdiButton::isSelected()
{
    return this->selected;
}

bool gdiButton::setSelected(bool selected)
{
    this->selected = selected;
    return this->selected;
}

bool gdiButton::isCard()
{
    return this->card;
}
