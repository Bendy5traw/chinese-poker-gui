// ChinesePoker.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "ChinesePoker.h"

// AI Strategy ideas:
// Power ranking system to figure out strength of hand in comparison to potential hands by other players
// --> Keep highly ranked hands from being played early
// Determining guaranteed or likely freebies to sequence hands so weak cards aren't left to be played last

bool isSameCard(cardObject card1, cardObject card2)
{
    return ((card1.getValue() == card2.getValue()) && (card1.getSuit() == card2.getSuit()));
}

cardObject getRandomCard()
{
    cardObject randomCard;
    randomCard.setValues(std::rand() % 13, std::rand() % 4);
    return randomCard;
}

std::vector<cardObject> globalHands::hand1 = std::vector<cardObject>();
std::vector<cardObject> globalHands::hand2 = std::vector<cardObject>();
std::vector<cardObject> globalHands::hand3 = std::vector<cardObject>();
std::vector<cardObject> globalHands::hand4 = std::vector<cardObject>();
std::vector<std::vector<cardObject>>* globalHands::handHistory = new std::vector<std::vector<cardObject>>;
std::vector<std::vector<cardObject>*> globalHands::allPlayerHands;

void dealCards()
{
    cardObject deck[52];
    auto deckSize = (sizeof(deck) / sizeof(cardObject));

    for (int i = 0; i < deckSize; i++)
    {
        bool insertCard = true;

        cardObject newCard = getRandomCard();
        for (int j = 0; j < i; j++)
        {
            if (isSameCard(newCard, deck[j]))
            {
                i--;
                insertCard = false;
                break;
            }
        }

        if (insertCard)
            deck[i] = newCard;
    }

    for (int i = 0; i < deckSize; i++)
    {
        if (i < deckSize / 4)
            globalHands::hand1.push_back(deck[i]);
        else if (i < (deckSize / 4) * 2)
            globalHands::hand2.push_back(deck[i]);
        else if (i < (deckSize / 4) * 3)
            globalHands::hand3.push_back(deck[i]);
        else
            globalHands::hand4.push_back(deck[i]);
    }

    sortHand(globalHands::hand1);
    sortHand(globalHands::hand2);
    sortHand(globalHands::hand3);
    sortHand(globalHands::hand4);

    globalHands::allPlayerHands.push_back(&globalHands::hand1);
    globalHands::allPlayerHands.push_back(&globalHands::hand2);
    globalHands::allPlayerHands.push_back(&globalHands::hand3);
    globalHands::allPlayerHands.push_back(&globalHands::hand4);
}

comboTypes isValidTurn(std::vector<cardObject> turn, bool starting, cardTypes validTypes)
{
    std::vector<comboTypes> foundHands;
    foundHands.push_back(ROYAL_FLUSH);
    foundHands.push_back(BOMB);
    foundHands.push_back(FULL_HOUSE);
    foundHands.push_back(STRAIGHT);
    foundHands.push_back(FLUSH);

    cardObject startingCard;
    startingCard.setValues(1, 0);

    if (turn.size() < 1)
        return INVALID;

    if (starting && !handContainsCard(turn, startingCard)) // make sure 3 of diamonds is played if starting
        return INVALID;

    int tempSuit = turn.at(0).getSuit();
    int tempRank = turn.at(0).getValue();
    int identicalCards = 0;
    bool foundDouble = false;
    bool foundTriple = false;
    bool foundQuad = false;

    if (validTypes == FREEBIE)
    {
        if (turn.size() == 5)
            validTypes = COMBO;
        else if (turn.size() == 4)
            validTypes = PASS;
        else
            validTypes = (cardTypes)(turn.size() - 1);
    }

    switch (validTypes)
    {
    case SINGLE:
        return (comboTypes)(turn.size() == 1);
    case PAIR:
        return (comboTypes)(turn.size() == 2 && (turn.at(0).getValue() == turn.at(1).getValue()));
    case TRIPLE:
        return (comboTypes)(turn.size() == 3 && (turn.at(0).getValue() == turn.at(1).getValue()) && ((turn.at(1).getValue() == turn.at(2).getValue())));
    case COMBO:
        for (size_t i = 1; i < turn.size(); i++)
        {
            if (turn.at(i).getSuit() != tempSuit && vectorIndexOf(foundHands, FLUSH) != -1) // flush check
                foundHands.erase(foundHands.begin() + vectorIndexOf(foundHands, FLUSH));

            if (turn.at(i).getValue() != tempRank + 1 && vectorIndexOf(foundHands, STRAIGHT) != -1) // straight check
                foundHands.erase(foundHands.begin() + vectorIndexOf(foundHands, STRAIGHT));
            else
                tempRank++;

            if (turn.at(i).getValue() == turn.at(i - 1).getValue())
            {
                identicalCards++;
                if (identicalCards == 1)
                    identicalCards++;
            }
            else
            {
                if (identicalCards == 2)
                    foundDouble = true;
                else if (identicalCards == 3)
                    foundTriple = true;
                else if (identicalCards == 4)
                    foundQuad = true;
                identicalCards = 0;
            }
        }

        if (identicalCards == 2)
            foundDouble = true;
        else if (identicalCards == 3)
            foundTriple = true;
        else if (identicalCards == 4)
            foundQuad = true;
        identicalCards = 0;

        if (!(foundDouble && foundTriple)) // full house check
            foundHands.erase(foundHands.begin() + vectorIndexOf(foundHands, FULL_HOUSE));
        if (!foundQuad) // 4 of a kind(bomb) check
            foundHands.erase(foundHands.begin() + vectorIndexOf(foundHands, BOMB));

        if (!(vectorIndexOf(foundHands, FLUSH) != -1 && vectorIndexOf(foundHands, STRAIGHT) != -1)) // royal flush
            foundHands.erase(foundHands.begin());

        if (foundHands.size() > 0)
            return foundHands.front();
        else
            return INVALID;
    default:
        return INVALID;
    }
}

std::string cardToString(cardObject card)
{
    return std::string();
}

int findStartingPlayer(std::vector<std::vector<cardObject>*> hands)
{
    for (size_t i = 0; i < hands.size(); i++)
        for (size_t j = 0; j < hands.at(i)->size(); j++)
            if (hands.at(i)->at(j).getValue() == 1 && hands.at(i)->at(j).getSuit() == 0)
                return i + 1;
    return 0;
}

bool handContainsCard(std::vector<cardObject> hand, cardObject card)
{
    for (size_t i = 0; i < hand.size(); i++)
        if (isSameCard(card, hand.at(i)))
            return true;
    return false;
}

bool handContainsValue(std::vector<cardObject> hand, cardObject card)
{
    for (size_t i = 0; i < hand.size(); i++)
        if (card.getValue() == hand.at(i).getValue())
            return true;
    return false;
}

int vectorIndexOf(std::vector<comboTypes> input, comboTypes element)
{
    for (size_t i = 0; i < input.size(); i++)
        if (input.at(i) == element)
            return i;
    return -1;
}

int vectorIndexOf(std::vector<cardObject> hand, cardObject element)
{
    for (size_t i = 0; i < hand.size(); i++)
        if (isSameCard(element, hand.at(i)))
            return i;
    return -1;
}

// assuming hand history exists, will have a separate function if starting
// also assuming all hands in hand history are valid
// will not exclude cards that make up a double/triple, this will be done in the computers turn itself
std::vector<cardObject> lowestValidHandOfType(std::vector<cardObject> hand, cardTypes validTypes, bool ignoreHistory, bool ignoreTwos, bool breakDoubles)
{
    assert(globalHands::handHistory);
    //assert(validTypes != FREEBIE);

    std::vector<cardObject> returnHand = std::vector<cardObject>();
    std::vector<cardObject> storeRoyalFlush = std::vector<cardObject>();

    std::map<int, int> sameRanks, sameSuits;

    auto it = hand.begin();

    while (it != hand.end())
    {
        sameRanks[it->getValue()]++;
        sameSuits[it->getSuit()]++;

        if (ignoreTwos && it->getValue() == 0)
            it = hand.erase(it);
        else
            it++;
    }

    if (!hand.size()) // only cards left are twos, let calling function do checks for that and recall
        return returnHand;

    if (validTypes == FREEBIE) // play most amount of cards possible for now, can be improved upon with "power ranking" system
    {
        cardTypes recursiveType = FREEBIE;
        bool recursiveIgnoreTwos = true;

        while (!returnHand.size() && recursiveType >= SINGLE) // use recursion to loop through validTypes
        {
            recursiveType = (cardTypes)(recursiveType - 1);
            returnHand = lowestValidHandOfType(hand, recursiveType, true, recursiveIgnoreTwos);

            if (recursiveType == SINGLE && !returnHand.size())
            {
                recursiveIgnoreTwos = false; // no hands to play that don't have twos, so play a twos hand, loop back through
                recursiveType = FREEBIE;
            }
        }
        
        return returnHand;
    }

    if (validTypes < COMBO) // single, double, or triple
    {
        size_t idx = 0;

        // skip all cards that are less than previous hand and that are not contained in the correct hand size
        while (idx < hand.size())
        {
            bool historyCheck = (ignoreHistory) ? false : (hand.at(idx) < globalHands::handHistory->back().back() || hand.at(idx).getValue() == globalHands::handHistory->back().back().getValue());
            if (!(sameRanks.find(hand.at(idx).getValue())->second == validTypes + 1) 
                || ((cardIsInCombo(hand, hand.at(idx)) && validTypes == SINGLE) && (!(breakDoubles && !cardIsInCombo(hand, hand.at(idx), NON_COMBO)))) // validTypes + 1 gives hand size
                || historyCheck)
                idx++;
            else
                break;
        }

        // add correct number of calls to returnHand
        // dont remember why but I had to switch from iterating forwards to backwards
        if (idx + validTypes < hand.size())
            for (size_t i = idx + validTypes; i >= idx && i < hand.size(); i--) // must check for i in bounds despite looping backwards because the unsigned int will roll over to max value
                returnHand.push_back(hand.at(i));

    }
    else // combo, work from lowest to highest
    {
        // TODO: make optimal version that returns hand that breaks up the least other combos instead of just the lowest
        comboTypes prevType = (!ignoreHistory) ? isValidTurn(globalHands::handHistory->back(), false, FREEBIE) : NON_COMBO;
        //bool isRecursiveFreebie = !(globalHands::handHistory->back().size() == 5); // this should only be true in a freebie

        // lowest straight detection
        if (prevType <= STRAIGHT || ignoreHistory)
        {
            bool inStraight = false;
            int straightLength = 0;
            size_t startIdx = -1;
            int doublesIncluded = 0;
            for (size_t i = 0; i < hand.size(); i++)
            {
                if (i > 0) // straight check
                {
                    if (doublesIncluded > 2) // dont break up more than 2 doubles
                        break;

                    if (hand.at(i).getValue() - hand.at(i - 1).getValue() <= 1)
                    {
                        if (sameRanks.find(hand.at(i).getValue())->second > 2) // dont break up triples or quads
                        {
                            inStraight = false;
                            startIdx = -1;
                            straightLength = 0;
                            if ((i + (size_t)sameRanks.find(hand.at(i).getValue())->second - 1) < hand.size())
                                i += (size_t)sameRanks.find(hand.at(i).getValue())->second - 1;
                            continue;
                        }
                        else if (sameRanks.find(hand.at(i).getValue())->second == 2)
                            doublesIncluded++;

                        inStraight = true;
                        if (startIdx == -1)
                            startIdx = i - 1;
                        if (hand.at(i).getValue() - hand.at(i - 1).getValue() == 1)
                            straightLength++;
                        if (straightLength == 1)
                            straightLength++;
                    }
                    else if (inStraight && straightLength >= 5
                        && (ignoreHistory ||
                            (prevType != STRAIGHT || globalHands::handHistory->back().back() < hand.at(i - 1))))
                    {
                        for (size_t j = startIdx; j < i; j++)
                            if (!handContainsValue(returnHand, hand.at(j)) && returnHand.size() < 5)
                                returnHand.push_back(hand.at(j));
                    }
                    else
                    {
                        inStraight = false;
                        straightLength = 0;
                        startIdx = -1;
                    }
                }
            }
            // check again at end
            if (inStraight && straightLength >= 5
                && (ignoreHistory ||
                    (prevType != STRAIGHT || globalHands::handHistory->back().back() < hand.back())))
            {
                for (size_t j = startIdx; j < hand.size(); j++)
                    if (!handContainsValue(returnHand, hand.at(j)) && returnHand.size() < 5)
                        returnHand.push_back(hand.at(j));
            }
            if (returnHand.size() == 5 && isValidTurn(returnHand, false, FREEBIE) == ROYAL_FLUSH)
            {
                storeRoyalFlush = returnHand;
                returnHand.clear();
            }
        }

        if (!ignoreHistory) // two if statements to avoid referencing handHistory on the first turn when it's empty
            if (returnHand < globalHands::handHistory->back())
                returnHand.clear();

        if (returnHand.size() < 5 && (prevType <= FLUSH || ignoreHistory))
        {
            for (int suit = (!ignoreHistory && prevType == FLUSH) ? globalHands::handHistory->back().front().getSuit() : 0; suit < 4; suit++) // flush
            {
                if (!sameSuits[suit]) // if there are no cards of this suit, checking for second will give an exception so you need to nullptr check
                    continue;
                if (sameSuits.find(suit)->second >= 5) // found lowest flush
                {
                    for (size_t i = 0; i < hand.size(); i++)
                        // only add stray cards first
                        if (hand.at(i).getSuit() == suit && !cardIsInCombo(hand, hand.at(i), FLUSH))
                            returnHand.push_back(hand.at(i));

                    while (returnHand.size() > 5)
                        returnHand.pop_back();

                    if (returnHand.size() < 5)
                        for (size_t i = 0; i < hand.size(); i++)
                            // on 2nd loop, only break up doubles
                            if (hand.at(i).getSuit() == suit && !(sameRanks.find(hand.at(i).getValue())->second > 2) && !handContainsCard(returnHand, hand.at(i)) && returnHand.size() < 5)
                                returnHand.push_back(hand.at(i));

                    if (returnHand.size() < 5) // breaking up a higher 5 card combo or triple, move on
                        returnHand.clear();
                    else if (isValidTurn(returnHand, false, FREEBIE) == ROYAL_FLUSH) // make sure its not a straight flush, this will be handled later
                    {
                        storeRoyalFlush = returnHand;
                        returnHand.clear();
                    }
                }
            }
        }

        if (!ignoreHistory) 
            if (returnHand < globalHands::handHistory->back())
                returnHand.clear();

        if (returnHand.size() < 5 && (prevType <= FULL_HOUSE || ignoreHistory))
        {
            cardObject refCard;
            if (!ignoreHistory)
            {
                if (globalHands::handHistory->back().front().getValue() == globalHands::handHistory->back().at(1).getValue() && globalHands::handHistory->back().at(1).getValue() == globalHands::handHistory->back().at(2).getValue())
                    refCard = globalHands::handHistory->back().front();
                else
                    refCard = globalHands::handHistory->back().back();
            }
            else
                refCard.setValues(1, 0);

            for (size_t i = 0; i < hand.size(); i++)
            {
                if (sameRanks.find(hand.at(i).getValue())->second == 3 && strayDouble(hand, hand.at(i)) && returnHand.size() != 3 && (refCard < hand.at(i) || ignoreHistory) && returnHand.size() < 5)
                {
                    for (size_t j = i; j < i + 3; j++)
                        returnHand.push_back(hand.at(j));
                    i += 2;
                }
                else if (sameRanks.find(hand.at(i).getValue())->second == 2 && returnHand.size() != 2 && returnHand.size() < 5)
                {
                    for (size_t j = i; j < i + 2; j++)
                        returnHand.push_back(hand.at(j));
                    i++;
                }
                if (returnHand.size() == 5)
                    break;
            }

            if (returnHand.size() < 5)
                returnHand.clear();
        }

        if (!ignoreHistory)
            if (returnHand < globalHands::handHistory->back())
                returnHand.clear();

        if (returnHand.size() < 5 && (prevType <= BOMB || ignoreHistory))
        {
            for (size_t i = 0; i < hand.size(); i++)
            {
                if (sameRanks.find(hand.at(i).getValue())->second == 4 && returnHand.size() != 4)
                    for (size_t j = i; j < i + 4; j++)
                        returnHand.push_back(hand.at(j));

                else if (!cardIsInCombo(hand, hand.at(i)) && returnHand.size() != 1 && returnHand.size() != 5) // dont break up anything if its not needed
                    returnHand.push_back(hand.at(i));

                if (returnHand.size() == 5) // this will cause errors in the rare chance it happens on the first turn, fix pls
                {
                    if (!ignoreHistory)
                    {
                        if (globalHands::handHistory->back() < returnHand)
                            break;
                        else
                            returnHand.clear(); // previous hand was higher bomb
                    }
                    else
                        break;
                }
            }

            comboTypes loopExclude = NON_COMBO;
            while (returnHand.size() == 4 && loopExclude < BOMB) // couldnt find 1 stray card, just break up lowest non combo instead
            {
                for (size_t i = 0; i < hand.size(); i++)
                    if (!cardIsInCombo(hand, hand.at(i), loopExclude) && !handContainsCard(returnHand, hand.at(i)))
                        returnHand.push_back(hand.at(i));
                loopExclude = (comboTypes)((int)loopExclude + 1);
            }

            if (returnHand.size() < 5)
                returnHand.clear();
        }

        if (!ignoreHistory)
            if (returnHand < globalHands::handHistory->back())
                returnHand.clear();

        if (returnHand.size() < 5 && storeRoyalFlush.size() == 5)
        {
            if (!ignoreHistory)
            {
                if (returnHand > globalHands::handHistory->back())
                    returnHand = storeRoyalFlush;
            }
            else
                returnHand = storeRoyalFlush;
        }
    }

    sortHand(returnHand);
    return returnHand;
}

// INCOMPLETE
std::vector<cardObject> highestHandOfType(std::vector<cardObject> hand, cardTypes validTypes)
{
    return std::vector<cardObject>();
}

// hand must be that of the starting players
std::vector<cardObject> takeStartingTurn(std::vector<cardObject> hand)
{
    assert(hand.front().getValue() == 1 && hand.front().getSuit() == 0); // must have 3 of diamonds, hand must be sorted

    // cant just check lowest hands because the three of diamonds might be contained in a higher hand

    if (!globalHands::handHistory)
        globalHands::handHistory = new std::vector<std::vector<cardObject>>;

    // attempting to simplify by using freebie logic first to maybe find a hand
    std::vector<cardObject> returnHand = std::vector<cardObject>();
    returnHand.push_back(hand.front());

    if (!cardIsInCombo(hand, hand.front())) // isnt contained in any hand, play single card
        return returnHand;

    returnHand = lowestValidHandOfType(hand, FREEBIE, true, true);

    if (returnHand.size() == 5)
    {
        if (!handContainsCard(returnHand, hand.front()) || returnHand.back().getValue() == 0) // never use 2s on starting hand
            returnHand.clear();
        else
            return returnHand;
    }
    else // while this could be the correct hand to return, it could also be something else so ill do 2 separate checks
        returnHand.clear();

    // by this point we should know for a fact that there are no 5 card combos with the 3 of diamond

    std::map<int, int> sameRanks, sameSuits;

    for (cardObject card : hand)
    {
        sameRanks[card.getValue()]++;
        sameSuits[card.getSuit()]++;
    }

    int numThrees = sameRanks[1];

    for (size_t i = sameRanks[1] - 1; i > 0; i--)
    {
        if (cardIsInCombo(hand, hand.at(i), NON_COMBO))
        {
            hand.erase(hand.begin() + i);
            numThrees--;
        }
    }

    for (size_t i = 0; i < numThrees; i++)
    {
        returnHand.push_back(hand.at(i));
    }

    return returnHand;
}

// assuming card is in hand, includes doubles and triples
// setting cExclude will make the function ignore that combo type
bool cardIsInCombo(std::vector<cardObject> hand, cardObject card, comboTypes cExclude)
{
    if (hand.size() == 1) // 1 card
        return false;

    if (cardIsInDbl(hand, card) && cExclude != NON_COMBO)
        return true;

    int sSuits = 0, sRanks = 0;
    bool inStraight = false, containsCard = false;
    int straightLength = 0;

    for (size_t i = 0; i < hand.size(); i++)
    {
        if (hand.at(i).getSuit() == card.getSuit() && !isSameCard(hand.at(i), card)) // count suits for flush
            sSuits++;
        if (hand.at(i).getValue() == card.getValue() && !isSameCard(hand.at(i), card)) // count suits for flush
            sRanks++;

        if (i > 0) // straight check
        {
            if (hand.at(i).getValue() - hand.at((size_t)i - 1).getValue() <= 1)
            {
                inStraight = true;
                if (hand.at(i).getValue() - hand.at((size_t)i - 1).getValue() == 1)
                    straightLength++;
                if (straightLength == 1)
                    straightLength++;
                if (isSameCard(hand.at(i), card) || (inStraight && i == 1 && isSameCard(hand.front(), card)))
                    containsCard = true;
            }
            else if (inStraight && containsCard && straightLength >= 5 && cExclude != STRAIGHT)
                return true;
            else
            {
                inStraight = false;
                containsCard = false;
                straightLength = 0;
            }
        }
    }

    // check again for straight, could be at the end of hand
    if (inStraight && containsCard && straightLength >= 5 && cExclude != STRAIGHT)
        return true;
    if (sSuits >= 5 && cExclude != FLUSH) // flush
        return true;

    if (cExclude != FULL_HOUSE && sRanks == 3 && strayDouble(hand, card))
        return true;
    if (cExclude != BOMB && sRanks == 4 && hand.size() > 4)
        return true;

    return false;
}

// also includes triples and quads
bool cardIsInDbl(std::vector<cardObject> hand, cardObject card)
{
    size_t cardIndex = vectorIndexOf(hand, card);

    if (hand.size() == 1) // 1 card
        return false;

    if (cardIndex < hand.size() - 1) // double/triple/quad check
        if (hand.at(cardIndex).getValue() == hand.at(cardIndex + 1).getValue())
            return true;
    if (cardIndex > 0) // double/triple/quad check
        if (hand.at(cardIndex).getValue() == hand.at(cardIndex - 1).getValue())
            return true;
    return false;
}

bool strayDouble(std::vector<cardObject> hand, cardObject excludeCard, bool triple)
{
    std::map<int, int> sameRanks;

    for (cardObject card : hand)
        sameRanks[card.getValue()]++;
    for (cardObject card : hand)
        if (sameRanks.find(card.getValue())->second == (triple ? 3 : 2) && card.getValue() != excludeCard.getValue())
            return true;
    return false;
}

bool operator<(std::vector<cardObject> hand1, std::vector<cardObject> hand2)
{
    std::vector<cardObject> sortedHand1 = hand1;
    std::vector<cardObject> sortedHand2 = hand2;
    sortHand(sortedHand1);
    sortHand(sortedHand2);
    comboTypes hand1Type = isValidTurn(hand1, false, FREEBIE);
    comboTypes hand2Type = isValidTurn(hand2, false, FREEBIE);
    cardObject refCard1;
    cardObject refCard2;
    if (hand1Type == hand2Type)
    {
        switch (hand1Type)
        {
        case NON_COMBO:
            if (hand1.size() != hand2.size())
                return (hand1.size() < hand2.size());
            else
                return (hand1.back() < hand2.back());
        case FLUSH:
            if (hand1.front().getSuit() != hand2.front().getSuit())
                return (hand1.front().getSuit() < hand2.front().getSuit());
            else
                return (hand1.back() < hand2.back());
        case FULL_HOUSE:
            if (hand1.front().getValue() == hand1.at(1).getValue() && hand1.at(1).getValue() == hand1.at(2).getValue())
                refCard1 = hand1.front();
            else
                refCard1 = hand1.back();
            if (hand2.front().getValue() == hand2.at(1).getValue() && hand2.at(1).getValue() == hand2.at(2).getValue())
                refCard2 = hand2.front();
            else
                refCard2 = hand2.back();
            return (refCard1 < refCard2);
        case BOMB:
            if (hand1.front().getValue() == hand1.at(1).getValue())
                refCard1 = hand1.front();
            else
                refCard1 = hand1.back();
            if (hand2.front().getValue() == hand2.at(1).getValue())
                refCard2 = hand2.front();
            else
                refCard2 = hand2.back();
            return (refCard1 < refCard2);
        default:
            return (hand1.back() < hand2.back());
        }
    }
    else
        return (hand1Type < hand2Type);
}

void sortHand(std::vector<cardObject>& hand)
{
    std::sort(hand.begin(), hand.end(), [](const cardObject& lhs, const cardObject& rhs) {
        return lhs < rhs;
        });
}